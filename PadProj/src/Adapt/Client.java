package Adapt;

import javax.swing.JOptionPane;

public class Client {

	public static void main(String[] args) {
		
		Target t = new Adapter();

		t.exibirMensagem("INF: Ola mundo ", JOptionPane.INFORMATION_MESSAGE); 
		t.exibirMensagem("ERR: Erro inesperado ", JOptionPane.ERROR_MESSAGE); 
		t.exibirMensagem("WARN:Cuidado!", JOptionPane.WARNING_MESSAGE); 
	}
}

