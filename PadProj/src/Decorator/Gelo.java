package Decorator;

public class Gelo extends Complemento {
	
	public Gelo(IBebida b){
		super(b);
	}
	
	@Override
	public String descricao() {
		return b.descricao() + ", " + "gelo";
	}
	
	@Override
	public double valor() {
		return b.valor() + 0.3;
	}
	
	@Override
	public String toString() {
		return "Valor: R$ " + valor() + " - " + descricao();
	}

}

