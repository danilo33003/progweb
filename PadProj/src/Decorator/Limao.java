package Decorator;

public class Limao extends Complemento {
	
	public Limao(IBebida b){
		super(b);
	}
	
	@Override
	public String descricao() {
		return b.descricao() + ", " + "limao";
	}
	
	@Override
	public double valor() {
		return b.valor() + 0.6;
	}
	
	@Override
	public String toString() {
		return "Valor: R$ " + valor() + " - " + descricao();
	}


}

