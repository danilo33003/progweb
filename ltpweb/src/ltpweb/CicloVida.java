package ltpweb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;  

@WebServlet("/cv")
public class CicloVida extends HttpServlet {
	
	private int contador;


	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado.");
		System.out.println("Contador inicial: " + contador);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
	  
		HttpSession session = req.getSession();
		session.setMaxInactiveInterval(10);
		
		out.println("<html>\r\n"
				+ "  <head>\r\n"
				+ "    <title>This is a title</title>\r\n"
				+ "  </head>\r\n"
				+ "  <body>\r\n"
				+ "    <div>\r\n"
				+ "        <a>LOGUIN</a>\r\n"
				+ "        <br/><br/>\r\n"
				+ "    </div>\r\n"
				+ "    <br/>\r\n"
				+ "    <div>\r\n"
				+ "    	<form action=\"cv\" method=\"get\">\r\n"
				+ "    		<label>USUARIO</label>\r\n"
				+ "    		<input type=\"text\" name=\"Usuario\" placeholder=\"Digite usuario valido\">\r\n"
				+ "    		<br/><br/>\r\n"
				+ "    		<label>SENHA</label>\r\n"
				+ "    		<input type=\"password\" name=\"Senha\" placeholder=\"Digite senha valida\">\r\n"
				+ "    		<br/><br/>\r\n"
				+ "    		<input type=\"submit\" value=\"Entrar\">\r\n"
				+ "    	</form>\r\n"
				+ "    </div>\r\n"
				+ "  </body>\r\n"
				+ "</html> ");
		
		
		
		 String senha = req.getParameter ("Senha");
		 String usuario = req.getParameter ("Usuario");
		 if(usuario.equals("aluno") && senha.equals( "aluno")){
			 contador = contador + 1;
			 
			 req.setAttribute("cont", contador);
				
				RequestDispatcher dispacther = req.getRequestDispatcher("ResultadoUsuarioValido.jsp");
				dispacther.forward(req, resp);	

 
    			
    			
        }else{
     		
        	RequestDispatcher dispacther = req.getRequestDispatcher("ResultadoUsuarioInvalido.jsp");
			dispacther.forward(req, resp);

        }
	}
	
	@Override
	public void destroy() {
		System.out.println("Servlet destruido.");
		System.out.println("Contador final: " + contador);
		
	}
}
